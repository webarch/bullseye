# Debian Bullseye

See the [Debian Bullseye upgrade
documentation](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-upgrading.en.html).

This role defaults to not removing non-Buster packages, not disabling files in
`/etc/apt/sources.list.d` and changing _"buster"_ to _"bullseye"_ in files in
`/etc/apt/sources.list.d`, this behaviour can be altered by changing the
[defaults](defaults/main.yml).

This role includes the [upgrade role](https://git.coop/webarch/upgrade) to
ensure that everything is upto date to start with.

This role includes the [apt role](https://git.coop/webarch/apt) to update the
`/etc/apt/sources.list` and run `apt-get --allow-releaseinfo-change update`.

## Servers with a /chroot

Ensure that there is enough spare space in the chroot and then upgrade the
`/chroot`, this doesn't require a reboot, use the
[bullseye.sh](https://git.coop/webarch/chroot-config/-/blob/master/bullseye.sh)
script for this:

```bash
cd /root/chroot-config
git pull
./bullseye.sh --no-check
```

Then `chroot /chroot` and run:

```bash
apt autoremove && apt clean && apt update && \
apt upgrade --without-new-pkgs && \
apt full-upgrade && \
apt purge libapache2-mod-php libapache2-mod-php7.3 libapache2-mod-php7.4 && \
apt autoremove
```

And then run the `bullseye.sh` script again from `/root/chroot_config` to
remove old packages and configure everything:

```bash
./bullseye.sh --no-check && \
./run.sh
```

# WSH Servers

Run this role to update the apt `sources.list` files:

```bash
export SERVERNAME="webarch1.co.uk"
ansible-playbook bullseye.yml -vv -l $SERVERNAME
```

Then check there is space to upgrade the server:

```bash
apt autoremove && apt clean && apt update && \
apt -o APT::Get::Trivial-Only=true full-upgrade
```

**Make sure there is at least 1GiB available for `/`.**

And then if there is do the upgrade:

```bash
apt upgrade --without-new-pkgs && \
apt full-upgrade && \
apt purge libapache2-mod-php libapache2-mod-php7.3 libapache2-mod-php7.4 && \
apt install yara && \
apt autoremove && \
rm -f /var/lib/munin-node/plugin-state/nobody/plugin-apt_all.state && \
cd /etc/munin/plugins && \
munin-run apt_all
```

For servers not running Apache:

```bash
apt upgrade --without-new-pkgs && \
apt full-upgrade && \
apt autoremove && \
rm -f /var/lib/munin-node/plugin-state/nobody/plugin-apt_all.state && \
cd /etc/munin/plugins && \
munin-run apt_all
```

After the upgrade reboot the server and  re-run this role and then re-generate
the PHP-FPM pool.d config for 7.4, and then the apache config and then upgrade
MariaDB and Exim:


```bash
export SERVERNAME="webarch1.co.uk"
ansible-playbook bullseye.yml  -vv -l $SERVERNAME && \
ansible-playbook wsh.yml -t php -vv -l $SERVERNAME && \
ansible-playbook users.yml -vv --extra-vars "users_update_strategy=phpfpm" -l $SERVERNAME && \
ansible-playbook wsh.yml -t apache -vv -l $SERVERNAME && \
ansible-playbook users.yml --extra-vars "users_update_strategy=apache" -vv -l $SERVERNAME && \
ansible-playbook wsh.yml -t mariadb,exim -vv -l $SERVERNAME && \
ansible-playbook wsh.yml -vv -l $SERVERNAME
```

Or for Nextcloud servers:

```bash
export SERVERNAME="webarch1.co.uk"
ansible-playbook bullseye.yml  -vv -l $SERVERNAME && \
ansible-playbook nextcloud.yml -t php -vv -l $SERVERNAME && \
ansible-playbook nextcloud.yml -t users -vv --extra-vars "users_update_strategy=phpfpm" -l $SERVERNAME && \
ansible-playbook nextcloud.yml -t apache -vv -l $SERVERNAME && \
ansible-playbook nextcloud.yml -t users --extra-vars "users_update_strategy=apache" -vv -l $SERVERNAME && \
ansible-playbook nextcloud.yml -t mariadb,exim -vv -l $SERVERNAME && \
ansible-playbook nextcloud.yml -vv -l $SERVERNAME
```

Then upgrade everything and all the users:

```bash
ansible-playbook wsh.yml -vv -l $SERVERNAME  && \
ansible-playbook users.yml -vv --extra-vars "users_update_strategy=all" -l $SERVERNAME 
```

## PostgreSQL

After upgrading the OS PostgreSQL need to be upgraded:

```bash
pg_dropcluster --stop 13 main
pg_upgradecluster 11 main
apt remove postgresql-11 postgresql-client-11
```

